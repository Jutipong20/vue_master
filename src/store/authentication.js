import api from "@/services/api.js";
import router from "@/router";

const authentication = {
  state: {
    Islogin: false,
    UserNmae: null,
  },
  getters: {
    get_IsLogin(state) {
      return state.Islogin;
    },
    get_UserName(state) {
      return state.UserNmae;
    },
  },
  mutations: {
    SET_ISLOGIN(state) {
      state.Islogin = true;
    },
    SET_LOGOUT(state) {
      state.Islogin = false;
    },
    SET_USERNAME(state, UserNmae) {
      state.UserNmae = UserNmae;
    },
  },
  actions: {
    async CheckLogin({ commit, dispatch }) {
      let result = await api.checkLogin();
      if (result.Success === true) {
        commit("SET_ISLOGIN");
        commit("SET_USERNAME", result.UserName);
        return true;
      } else {
        dispatch("LogOut", {});
        return false;
      }
    },
    async Login({ commit, dispatch }, { UserName, Password }) {
      let result = await api.login({ UserName, Password });
      if (result == true) {
        commit("SET_ISLOGIN");
        commit("SET_USERNAME", UserName);
        router.push("/Home").catch(() => {});
      } else {
        dispatch("LogOut", {});
      }
    },
    LogOut({ commit }) {
      api.logoff();
      commit("SET_LOGOUT");
      commit("SET_USERNAME", "");
      localStorage.clear();
      router.push("/Login").catch(() => {});
    },
  },
  modules: {},
};

export default authentication;
