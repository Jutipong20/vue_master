// import { server } from "@/services/constants";

const getMenus = async () => {
  //mock datas
  let menuList = [
    {
      icon: "mdi-contacts",
      title: "Main",
      active: true,
      childrens: [
        { icon: "mdi-plus", route: "Home", active: true },
        { icon: "mdi-plus", route: "About", active: false }
      ]
    },
    {
      icon: "mdi-contacts",
      title: "Labels",
      active: false,
      childrens: [
        { icon: "mdi-plus", route: "Create_label_1", active: false },
        { icon: "mdi-plus", route: "Create_label_2", active: false },
        { icon: "mdi-plus", route: "Create_label_3", active: false }
      ]
    },
    {
      icon: "mdi-cellphone-link",
      title: "More",
      active: false,
      childrens: [
        { icon: "mdi-settings", route: "Import", active: false },
        { icon: "mdi-settings", route: "Export", active: false },
        { icon: "mdi-settings", route: "Print", active: false },
        { icon: "mdi-settings", route: "Undo changes", active: false },
        { icon: "mdi-settings", route: "Other contacts", active: false }
      ]
    }
  ];

  return menuList;
};

export default {
  getMenus
};
