// import httpClient from "@/services/httpClient";
import { server } from "@/services/constants";
import api_menu from "./layout/menu_api";

const checkLogin = async () => {
  let result = { Success: false, UserName: null };
  let token = localStorage.getItem(server.TOKEN_KEY);
  if (token != null) {
    result.Success = true;
    result.UserName = localStorage.getItem(server.USERNAME);
    return result;
  } else {
    return result;
  }
};

const login = async (values) => {
  // wait api
  if (values.UserName === "admin" && values.Password === "1234") {
    localStorage.setItem(server.USERNAME, values.UserName);
    localStorage.setItem(server.TOKEN_KEY, server.TOKEN_KEY);
    return true;
  } else {
    return false;
  }
};

const logoff = () => {
  localStorage.removeItem(server.TOKEN_KEY);
};

export default {
  logoff,
  login,
  checkLogin,
  ...api_menu,
};
